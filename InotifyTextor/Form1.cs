﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InotifyTextor
{


    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> lines = new List<string>();
            string instr = txtsrc.Text;


            //uncomment to remove all new lines and tabs etc.
            //RemoveNTR(instr);


            lines = instr.Split('\n').ToList<string>();

            foreach (string line in lines)
            {
                List<string> props = new List<string>();
                props = line.Split(' ').ToList<string>();


                string type = props[0];
                string BIGname = props[1];
                string SMname = BIGname.ToLower();

                string oneprop = "";
                oneprop += String.Format("private {0} _{1};\n",type,SMname);
                oneprop += String.Format("public {0} {1}\n",type,BIGname);
                oneprop += String.Format("{{\n\t get \n\t {{ \n\t\t return _{0};\n\t}}\n",SMname);
                oneprop += String.Format("\t set\n\t{{ \n\t\t if(value != _{0}) \n",SMname);
                oneprop += String.Format("\t\t{{\n\t\t\t_{0} = value;\n\t\t\tNotifyPropertyChanged(\"{1}\");\n\t\t}}\n", SMname,BIGname);
                oneprop += String.Format("\t}}\n}}\n");

                txtout.Text += oneprop;
                txtout.Text += "\n\n=================================\n\n";
                
            }

        }

        private string RemoveNTR (string s)
        {
            var sb = new StringBuilder(s.Length);

            foreach (char i in s)
                if (i != '\n' && i != '\r' && i != '\t')
                    sb.Append(i);

            s = sb.ToString();

            return s;
        }
    }
}
